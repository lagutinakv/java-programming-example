package triangle;

import java.util.Scanner;

public class Triangle {
    public static double enterNum() {
        double x = -1;
        Scanner s = new Scanner(System.in);
        while (x <= 0) {
            if (s.hasNextDouble()) {
                x = s.nextDouble();
                if (x > 0) {
                    return x;
                } else {
                    System.out.println("Negative! Re-enter the side!");
                }
            } else {
                System.out.println("It is not number! Re-enter the side!");
                String str = s.nextLine();
                continue;
            }
        }
        return x;
    }

    public static boolean isTriangle(double a, double b, double c) {
        if ((a < b + c) && (b < c + a) && (c < a + b)) {
            return true;
        }
        return false;
    }

    public static double area(double a, double b, double c) {
        double p = ((a + b + c) / 2);
        double S = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        return S;
    }

    public static double radius(double a, double b, double c) {
        double R = (a * b * c) / (4 * area(a, b, c));
        return R;
    }


    public static void main(String[] args) {

        System.out.println("Enter sides of the triangle:");
        System.out.print("a = ");
        double a = enterNum();
        System.out.print("b = ");
        double b = enterNum();
        System.out.print("c = ");
        double c = enterNum();
        if (!isTriangle(a, b, c)) {
            System.out.println("This is not a triangle");
            return;
        }
        System.out.println("Enter radius of the circle: ");
        double r = enterNum();
        double R = radius(a, b, c);
        if (r>= R) {
            System.out.println("Vhodit");
        } else {
            System.out.println("ne vhodit");
        }
    }
}
